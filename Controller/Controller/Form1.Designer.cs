﻿namespace Controller
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtStartupEXE = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chkAttemptTrading = new System.Windows.Forms.CheckBox();
            this.numListStart = new System.Windows.Forms.NumericUpDown();
            this.numListEnd = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown3 = new System.Windows.Forms.NumericUpDown();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numListStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numListEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).BeginInit();
            this.SuspendLayout();
            // 
            // txtStartupEXE
            // 
            this.txtStartupEXE.Location = new System.Drawing.Point(86, 12);
            this.txtStartupEXE.Name = "txtStartupEXE";
            this.txtStartupEXE.Size = new System.Drawing.Size(227, 20);
            this.txtStartupEXE.TabIndex = 0;
            this.txtStartupEXE.Text = "C:\\Users\\Scott\\Source\\Repos\\BalloonFight\\BalloonFight\\bin\\Debug\\BalloonFight.exe";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Startup EXE:";
            // 
            // chkAttemptTrading
            // 
            this.chkAttemptTrading.AutoSize = true;
            this.chkAttemptTrading.Location = new System.Drawing.Point(160, 39);
            this.chkAttemptTrading.Name = "chkAttemptTrading";
            this.chkAttemptTrading.Size = new System.Drawing.Size(101, 17);
            this.chkAttemptTrading.TabIndex = 2;
            this.chkAttemptTrading.Text = "Attempt Trading";
            this.chkAttemptTrading.UseVisualStyleBackColor = true;
            // 
            // numListStart
            // 
            this.numListStart.Location = new System.Drawing.Point(12, 38);
            this.numListStart.Name = "numListStart";
            this.numListStart.Size = new System.Drawing.Size(46, 20);
            this.numListStart.TabIndex = 3;
            // 
            // numListEnd
            // 
            this.numListEnd.Location = new System.Drawing.Point(64, 38);
            this.numListEnd.Name = "numListEnd";
            this.numListEnd.Size = new System.Drawing.Size(46, 20);
            this.numListEnd.TabIndex = 4;
            this.numListEnd.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // numericUpDown3
            // 
            this.numericUpDown3.Location = new System.Drawing.Point(267, 38);
            this.numericUpDown3.Name = "numericUpDown3";
            this.numericUpDown3.Size = new System.Drawing.Size(46, 20);
            this.numericUpDown3.TabIndex = 5;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 67);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(301, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "Start";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(325, 102);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.numericUpDown3);
            this.Controls.Add(this.numListEnd);
            this.Controls.Add(this.numListStart);
            this.Controls.Add(this.chkAttemptTrading);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtStartupEXE);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.numListStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numListEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtStartupEXE;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkAttemptTrading;
        private System.Windows.Forms.NumericUpDown numListStart;
        private System.Windows.Forms.NumericUpDown numListEnd;
        private System.Windows.Forms.NumericUpDown numericUpDown3;
        private System.Windows.Forms.Button button1;
    }
}

