﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Sulakore.Communication;
using Sulakore.Habbo.Headers;
using Sulakore.Habbo.Web;
using Sulakore.Protocol.Encryption;
using RestSharp;
using System.Net;
using System.Linq;
using System.Threading;
using System.Collections;

namespace BalloonFight
{
    class Program
    {
        public const int REAL_EXPONENT = 10001;
        public const string REAL_MODULUS = "e052808c1abef69a1a62c396396b85955e2ff522f5157639fa6a19a98b54e0e4d6e44f44c4c0390fee8ccf642a22b6d46d7228b10e34ae6fffb61a35c11333780af6dd1aaafa7388fa6c65b51e8225c6b57cf5fbac30856e896229512e1f9af034895937b2cb6637eb6edf768c10189df30c10d8a3ec20488a198063599ca6ad";

        static readonly object _clearWriteLock;
        static readonly List<Task<bool>> _loginTasks;
        static readonly IDictionary<HSession, HKeyExchange> _exchanges;
        static readonly IDictionary<Task<bool>, HSession> _sessionTasks;
        static List<string> _ssoTickets;

        static List<HSession> Sessions { get; }
        static List<HSession> ConnectedSessions { get; }
        static int Balloons = 0;

        static AutoResetEvent _BalloonsCounted = new AutoResetEvent(false);
        static bool AttemptTrading = true;
        static readonly int[] RaiderGroups = new int[] { 448491, 448492, 448483, 448493, 448486, 448490, 448480, 448479, 448489, 448484, 448494, 448475, 448482, 448481, 448485, 448488, 448478, 448487, 448495, 448476 };
        static readonly int[] RaiderRooms = new int[] { 70264141, 70264231, 70264241, 70264252, 70264258, 70264270, 70264279, 70264288, 70264308, 70264315, 70264320, 70264323, 70264328, 70264334, 70264342, 70264352, 70264361, 70264366, 70264369, 70264384, 70309408, 70309416, 70309425, 70309432, 70309442, 70309508, 70309525, 70309539, 70309548, 70309563, 70309569, 70309576, 70309581, 70309593, 70309598, 70309601, 70309609, 70309615, 70309625, 70309632, 70309636, 70309644, 70309646, 70309650, 70309658, 70309664 };
        static readonly int[] CollectorRooms = new int[] { 70277984, 70278838, 70278844, 70278849, 70282285, 70282298, 70282328, 70282400 };
        static readonly string[] CollectorNames = new string[] { "mootlicker", "PowerOfHFFM", "TTB1", "TTB2", "TTB7", "TTB18", "TTB8", "TTB19" };
        static int collectorRoom = 0;
        static int collectorId = 0;
        static int MaxParallel = 1;

        static Program()
        {
            _clearWriteLock = new object();
            _loginTasks = new List<Task<bool>>();
            _exchanges = new Dictionary<HSession, HKeyExchange>();
            _sessionTasks = new Dictionary<Task<bool>, HSession>();
            _ssoTickets = new List<string>();

            Sessions = new List<HSession>();
            ConnectedSessions = new List<HSession>();

            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Console.WriteLine(e.ExceptionObject.GetType());
        }

        static void Main(string[] args)
        {
            LoadHeaders();

            Console.Title = "Balloon Fight";

            var collection = new Dictionary<string, string> { { "53922135@xksc.org", "$y#n3T8#c0" } };

            var accounts = Enumerable.Empty<string>();
            if (args.Length == 2) {
                collectorRoom = int.Parse(args[1]);
                AttemptTrading = true;
                accounts = System.IO.File.ReadAllLines(@"C:\accounts.txt").Partition(500).ElementAt(int.Parse(args[0]));
            }
            else
            {
                AttemptTrading = false;
                accounts = accounts.Union(System.IO.File.ReadAllLines(@"C:\accounts.txt").Partition(250).ElementAt(int.Parse(args[0])));
            }

            Parallel.ForEach(accounts, new ParallelOptions() { MaxDegreeOfParallelism = 1 }, (account) =>
            {
                try
                {
                    var session = new HSession(account.Split('\t')[0], account.Split('\t')[1], Sulakore.HHotel.Com);
                    var sso = SSOTicket(account.Split('\t')[0], account.Split('\t')[1]);
                    session.Balloons = new List<int>();
                    sso.Wait();
                    session.SsoTicket = sso.Result;
                    new Task(async () =>
                    {
                        session.DataIncoming += Session_DataIncoming;
                        session.Connected += Session_Connected;
                        await session.ConnectAsync();
                    }).Start();

                    /*Task.Delay(4000).Wait();
                    session.SendToServerAsync(2274, "M", "sh-295-64.fa-1201-0.hr-170-42.hd-190-14.lg-285-82.ch-255-64.ha-1005-64").Wait(); // change clothes
                    Task.Delay(100).Wait();
                    session.SendToServerAsync(960, "Serialize").Wait(); // f/r Serialize
                    session.SendToServerAsync(1442).Wait();
                    Task.Delay(100).Wait();
                    session.SendToServerAsync(960, "Iterator").Wait(); // f/r Iterator
                    session.SendToServerAsync(1442).Wait();
                    Task.Delay(100).Wait();
                    session.SendToServerAsync(921, 423377).Wait(); // join h4v0ck
                    session.SendToServerAsync(921, 443331).Wait(); // join Bookie
                    session.SendToServerAsync(921, 447373).Wait(); // join Balloon Poppers*/

                    /*Task.Delay(3000).Wait();
                    foreach (int balloon in session.Balloons) Task.Delay(1000);
                    Task.Delay(9600).Wait();*/

                    Task.Delay(240000).Wait();

                    //session.Connected -= Session_Connected;
                    //session.DataIncoming -= Session_DataIncoming;
                    //session.Disconnect();
                }
                catch { }
            });

            Console.Read();
        }

        static void LoadHeaders()
        {
            Outgoing.Global.InitiateHandshake = 2994; //required
            Outgoing.Global.ClientPublicKey = 1021; //required
            //Outgoing.Global.FlashClientUrl = 2988; //required?????
            Outgoing.Global.MachineId = 3364; //required
            Outgoing.Global.ClientSsoTicket = 3248; //required
            Outgoing.Global.HotelViewEvent = 2823; // date,event (2015-06-01 00:00,habbo15launch)
            Outgoing.Global.LoadInventory = 302;
            Outgoing.Global.NavigateRoom = 3966;

            // it'll fuck up the code if you comment those out. o god... don't ask.

            Outgoing.Global.TradePlayer = 88;
            Outgoing.Global.AcceptTrade = 536;
            Outgoing.Global.ConfirmTrade = 3443;
        }

        private static void Session_Connected(object sender, EventArgs e)
        {
            var session = (HSession)sender;
            session.Connected -= Session_Connected;

            session.SendToServer(Outgoing.CLIENT_CONNECT,
                "PRODUCTION-201507142058-777611711", "FLASH", 1, 0);

            session.SendToServer(Outgoing.Global.InitiateHandshake);
        }
        private static void Session_DataIncoming(object sender, InterceptedEventArgs e)
        {
            Console.WriteLine("-----------------------------");
            Console.WriteLine($"[{e.Packet.Header}] <- {e.Packet.ToString()}");
            var session = (HSession)sender;
            switch (e.Step)
            {
                case 1:
                    {
                        var exchange = new HKeyExchange(REAL_EXPONENT, REAL_MODULUS);
                        _exchanges.Add(session, exchange);
// I mean VS crashing >_> o lol yeah might be
                        exchange.DoHandshake(e.Packet.ReadString(), e.Packet.ReadString());
                        session.SendToServer(Outgoing.Global.ClientPublicKey, exchange.GetPublicKey());
                        break;
                    }
                case 2:
                    {
                        byte[] key = _exchanges[session].GetSharedKey(e.Packet.ReadString());
                        session.Remote.Encrypter = new Rc4(key);
                        session.Remote.Decrypter = new Rc4(key);

                        session.SendToServer(Outgoing.Global.FlashClientUrl, 401,
                            "https://habboo-a.akamaihd.net/gordon/PRODUCTION-201507142058-777611711/",
                            "https://www.habbo.com/gamedata/external_variables/29c0b40e73a6f3218752b49c726acfbe2111740d");

                        /*session.SendToServer(Outgoing.Global.MachineId,
                            "6ec1e92dee7c4a9db02ea6c44c9337ec0001b6b49fb42e6b7318ee437e68662bcb163527ede6",
                            "~4feab7a1118a50f484ef06cb8c538302");*/ //i aint sending that shit

                        Console.WriteLine(session.SsoTicket);

                        //session.SendToServer(Outgoing.Global.ClientSsoTicket,
                            //session.SsoTicket, 3407); if you're doing what I think you're doing, this will keep it from sending the SSO, so you can patch it into Kendux yourself.

                        break;
                    }
                case 3:
                    {
                        // it's the correct way to track the session.
                        if (e.Packet.Header != Incoming.SERVER_DISCONNECT)
                        {
                            //Write($"Connected: {session.SsoTicket}");
                            ConnectedSessions.Add(session);

                            Console.Title = $"BalloonFight | Authenticated: {Sessions.Count} | Connected: {ConnectedSessions.Count} | Balloons: {Balloons}";
                            //new Heartbeat(session).StartHeartbeat();
                            Task.Delay(1000).Wait();
                            //EnterRoom(session, RaiderRooms[new Random().Next(0, RaiderRooms.Length)], "h4v0ck");
                            Task.Delay(1000).Wait();
                            /*session.SendToServerAsync(193, 53922135).Wait();
                            Task.Delay(1000).Wait();
                            session.SendToServerAsync(193, 53922135).Wait();
                            Task.Delay(1000).Wait();
                            session.SendToServerAsync(193, 53922135).Wait();
                            Task.Delay(1000).Wait();*/
                            //session.SendToServerAsync(1967, "SafetyQuiz1").Wait();
                            Task.Delay(1000).Wait();
                            //EnterRoom(session, CollectorRooms[collectorRoom], "h4v0ck");
                            //new Task(() => LoopRooms(session, RaiderRooms)).Start();
                            //new TradingPass(session).StartWandering(4, 12, 1, 15);
                            //EnterRoom(session, 70157695);
                            //Task.Delay(3000).Wait();
                            //Task.Delay(1000).Wait();
                            //GeneralFunctions.ScratchPets(session);
                            //Task.Delay(1000).Wait();
                            //session.SendToServerAsync(1071);
                            //Task.Delay(1000).Wait();
                            //EnterRoom(session, 70157695);
                        }
                        break;
                    }
            }

            switch (e.Packet.Header)
            {
                case 2438:
                    session.SendToServer(Outgoing.Global.HotelViewEvent, "2013-05-08 13:00,gamesmaker;2013-05-11 13:00");
                    session.SendToServer(3394);
                    Task.Delay(200).Wait();
                    session.SendToServer(3394);
                    session.SendToServer(1869);
                    session.SendToServer(Outgoing.Global.HotelViewEvent, "2015-06-01 00:00,habbo15launch");
                    session.SendToServer(Outgoing.Global.HotelViewEvent, "2015-05-11 09:00,habbo15balloon;2015-07-07 15:00,habbo15rare;2015-07-08 15:00,habbo15balloon");
                    session.SendToServer(Outgoing.Global.HotelViewEvent, "2015-04-12 19:00,habbo15palooza");
                    session.SendToServer(Outgoing.Global.HotelViewEvent, false, false);
                    session.SendToServer(2846);
                    session.SendToServer(2366, "Login", "socket", "client.auth_ok", false, false, false, false, false, false);
                    session.SendToServer(3867);

                    session.SendToServer(1097);
                    session.SendToServer(1638);
                    session.SendToServer(198);
                    session.SendToServer(2099);
                    session.SendToServer(1670);
                    session.SendToServer(1739, "habbo_club");
                    session.SendToServer(1889);
                    session.SendToServer(244);
                    session.SendToServer(3197);

                    session.SendToServer(2630);
                    session.SendToServer(2968);
                    session.SendToServer(1473);
                    session.SendToServer(1585, "citizenship");
                    session.SendToServer(2265);

                    // Load Inventory
                    /*session.SendToServerAsync(Outgoing.Global.LoadInventory).Wait();
                    session.SendToServer(2242);*/
                    break;
                case 492:
                    if (AttemptTrading)
                    {
                        e.Packet.ReadInteger();
                        e.Packet.ReadInteger();
                        var name = e.Packet.ReadString();
                        if (CollectorNames.Contains(name))
                        {
                            e.Packet.ReadString();
                            e.Packet.ReadString();
                            collectorId = e.Packet.ReadInteger();
                            Console.WriteLine(collectorId);
                        }
                        e.Packet.Position = 0;
                    }
                    break;
                case 3810:
                    if (AttemptTrading)
                    {
                        foreach (var item in HInventory.Parse(session, e.Packet))
                            if (item.SpriteId == 8201 || item.SpriteId == 8193)
                            {
                                session.Balloons.Add(item.ItemId);
                                Balloons++;
                                Console.Title = $"BalloonFight | Authenticated: {Sessions.Count} | Connected: {ConnectedSessions.Count} | Balloons: {Balloons}";
                            }

                        Task.Delay(2000).Wait();
                        //RespectUser(session, 53922135);

                        if (session.Balloons.Count > 0)
                        {
                            Console.WriteLine(session.Email);
                            session.SendToServerAsync(88, collectorId).Wait();
                            Task.Delay(500).Wait();
                            foreach (var balloon in session.Balloons)
                            {
                                session.SendToServerAsync(48, balloon).Wait();
                                Task.Delay(600).Wait();
                            }

                            session.SendToServerAsync(Outgoing.Global.AcceptTrade).Wait(); // accept
                            Task.Delay(4000).Wait();
                            session.SendToServerAsync(Outgoing.Global.ConfirmTrade).Wait(); // confirm
                            Task.Delay(1000).Wait();
                        }
                    }
                    break;
            }
        }

        static void RespectUser(HSession session, int userId)
        {
            session.SendToServerAsync(193, userId).Wait();
            Task.Delay(1000).Wait();
        }

        static void LoopRooms(HSession session, int[] ids)
        {
            Random rnd = new Random();
            ids = ids.OrderBy(r => rnd.Next()).ToArray();
            while (true)
            {
                foreach (int id in ids)
                {
                    EnterRoom(session, id, "h4v0ck");
                    Task.Delay(60000).Wait();
                }
            }
        }

        static void EnterRoom(HSession session, int id, string password = "")
        {
            new Task(() =>
            {
                //session.SendToServerAsync(3427, "SafetyQuiz1").Wait();
                //Task.Delay(100).Wait();
                //session.SendToServerAsync(3704).Wait(); // Forget what this does...
                session.SendToServerAsync(Outgoing.Global.NavigateRoom, id, password, -1).Wait();
                //session.SendToServerAsync(1380, "Navigation", "", "go.official", $"{id}", 0).Wait();
                //Task.Delay(100).Wait();
                session.SendToServerAsync(3562).Wait();
                Task.Delay(100).Wait();
                session.SendToServerAsync(291).Wait();
                Task.Delay(100).Wait();
                session.SendToServerAsync(2305).Wait();
                Task.Delay(100).Wait();
                session.SendToServerAsync(2448).Wait();
                Task.Delay(100).Wait();
                session.SendToServerAsync(870).Wait();
                Task.Delay(100).Wait();
                session.SendToServerAsync(2013).Wait();
                Task.Delay(100).Wait();
                session.SendToServerAsync(3687, id, 1, 0).Wait();
                Task.Delay(100).Wait();
                session.SendToServerAsync(1929, 2).Wait();
            }).Start();
        }

        static void Write(string value)
        {
            value += "\r\n---------------";
            Console.WriteLine(value);
        }
        static void ClearWrite(string value)
        {
            lock (_clearWriteLock)
            {
                Console.Clear();
                Write(value);
            }
        }
        static async Task<string> SSOTicket(string email, string password)
        {
            var client = new RestClient("https://ex.habbo.com");
            client.CookieContainer = new CookieContainer();
            client.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.130 Safari/537.36";
            client.Get(new RestRequest("/"));

            var req = new RestRequest("/account/submit");
            req.AddParameter("credentials.username", email);
            req.AddParameter("credentials.password", password);

            var res = client.Post(req);

            if (res.Content.Contains("useOrCreateAvatar"))
            {
                var avatarID = res.Content.GetChild("http://ex.habbo.com/identity/useOrCreateAvatar/", '?');
                client.BaseUrl = new Uri("http://ex.habbo.com");
                var avatarFwd = client.Get(new RestRequest("/identity/useOrCreateAvatar/" + avatarID).AddQueryParameter("next", ""));
                client.BaseUrl = new Uri("https://ex.habbo.com");
                var clientSrc = client.Get(new RestRequest("/client")).Content;
                if (clientSrc.Contains("sso.ticket"))
                {
                    return clientSrc.GetChild("\"sso.ticket\" : \"", '"');
                }
            }

            return null;
        }
    }

    public static class EnumerableExtensions
    {
        public static IEnumerable<IEnumerable<TElement>> Partition<TElement>(this IEnumerable<TElement> @this, int partitionSize)
        {
            if (@this == null) throw new ArgumentNullException("this");

            return new PartitionedEnumerable<TElement>(@this, partitionSize);
        }

        private sealed class PartitionedEnumerable<TElement> : IEnumerable<IEnumerable<TElement>>
        {
            #region Public

            public PartitionedEnumerable(IEnumerable<TElement> elements, int partitionSize)
            {
                this.elements = elements;
                this.partitionSize = partitionSize;
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            public IEnumerator<IEnumerable<TElement>> GetEnumerator()
            {
                IEnumerator<TElement> elementEnumerator = this.elements.GetEnumerator();

                var partition = new List<TElement>();
                while (elementEnumerator.MoveNext())
                {
                    partition.Add(elementEnumerator.Current);

                    if (partition.Count == partitionSize)
                    {
                        yield return partition;
                        partition = new List<TElement>();
                    }
                }

                if (partition.Count > 0) yield return partition;
            }

            #endregion

            #region Private

            private readonly IEnumerable<TElement> elements;
            private readonly int partitionSize;

            #endregion
        }
    }
}