﻿using Sulakore.Habbo.Web;
using Sulakore.Protocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BalloonFight
{
    public class HInventory
    {
        public static List<HItem> Parse(HSession session, HMessage packet)
        {
            var items = new List<HItem>();

            var x1 = packet.ReadInteger();
            var x2 = packet.ReadInteger();

            int capacity = packet.ReadInteger();
            for (int i = 0; i < capacity; i++)
            {
                var item = new HItem();

                item.ItemId = packet.ReadInteger(); // this is a pos
                item.Type = packet.ReadString();
                packet.ReadInteger(); // this is a neg
                item.SpriteId = packet.ReadInteger();

                packet.ReadInteger();
                var a1 = packet.ReadInteger();
                
                #region Switch Statement: a1
                switch (a1)
                {
                    case 0: // _-2kH
                        {
                            packet.ReadString();
                            break;
                        }
                    case 1: // MapStuffData
                        {
                            int capacity2 = packet.ReadInteger();
                            for (int j = 0; j < capacity2; j++)
                            {
                                packet.ReadString();
                                packet.ReadString();
                            }
                            break;
                        }
                    case 2: // StringArrayStuffData()
                        {
                            int capacity3 = packet.ReadInteger();
                            for (int j = 0; j < capacity3; j++)
                            {
                                packet.ReadString();
                            }
                            break;
                        }
                    case 3: // _-1BL
                        {
                            packet.ReadString();
                            packet.ReadInteger();
                            break;
                        }
                    case 4: //_-6Zf
                        {
                            break;
                        }
                    case 5: // IntArrayStuffData
                        {
                            int capacity4 = packet.ReadInteger();
                            for (int j = 0; j < capacity4; j++)
                            {
                                packet.ReadInteger();
                            }
                            break;
                        }
                    case 6: // HighScoreStuffData
                        {
                            var y1 = packet.ReadString();
                            var y2 = packet.ReadInteger();
                            var y3 = packet.ReadInteger();

                            var capacity5 = packet.ReadInteger();
                            for (int j = 0; j < capacity5; j++)
                            {
                                var score = packet.ReadInteger();
                                var capacity6 = packet.ReadInteger();
                                for (int k = 0; k < capacity6; k++)
                                {
                                    var someString = packet.ReadString();
                                }
                            }

                            // NO BASE CALL
                            break;
                        }
                    case 7: // _-0TK
                        {
                            var k1 = packet.ReadString();
                            var k2 = packet.ReadInteger();
                            var k3 = packet.ReadInteger();
                            break;
                        }
                }
                #endregion

                if (a1 != 6)
                {
                    int flag = (a1 & 0xFF00);
                    if ((flag & 0x0100) > 0)
                    {
                        var h1 = packet.ReadInteger();
                        var h2 = packet.ReadInteger();
                    }
                }

                var v7 = packet.ReadBoolean();
                var v8 = packet.ReadBoolean();
                var v9 = packet.ReadBoolean();
                var v10 = packet.ReadBoolean();

                var v11 = packet.ReadInteger();
                var v12 = packet.ReadBoolean();
                var v13 = packet.ReadInteger();

                if (item.Type == "S")
                {
                    packet.ReadString();
                    packet.ReadInteger();
                }

                items.Add(item);
            }

            return items;
        }
    }

    public class HItem
    {
        public int ItemId { get; set; }
        public string Type { get; set; }
        public int SpriteId { get; set; }
    }
}
