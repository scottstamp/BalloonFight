﻿using Sulakore.Habbo.Web;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BalloonFight
{
    public class GeneralFunctions
    {
        private static Dictionary<string, int> Pets = new Dictionary<string, int> {
                { "King", 5375990 },
                { "Queen", 5376230 },
                { "Panda", 5376294 },
                { "Rawr", 5376272 },
                { "Mistletoe", 5376661 }
            };

        public static void ScratchPets(HSession session)
        {
            new Task(() =>
            {
                var rnd = new Random(session.GetHashCode());
                
                if (rnd.Next(1, 100) % 2 == 0)
                    session.SendToServerAsync(3763, Pets["Panda"]);
                else
                    session.SendToServerAsync(3763, Pets["Rawr"]);

                Task.Delay(1000).Wait();

            }).Start();
        }
    }
}
