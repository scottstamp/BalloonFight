﻿using System;
using System.ComponentModel;

namespace BalloonFight
{
    internal static class ExtensionMethods
    {
        internal static void SynchronizedInvoke(this ISynchronizeInvoke sync, Action action)
        {
            if (!sync.InvokeRequired) { action(); return; }
            sync.Invoke(action, new object[] { });
        }

        internal static string GetChild(this string body, string parent) =>
            body.Substring(body.IndexOf(parent, StringComparison.OrdinalIgnoreCase) + parent.Length).Trim();

        internal static string GetChild(this string body, string parent, char delimiter) =>
            GetChilds(body, parent, delimiter, false)[0].Trim();

        internal static string[] GetChilds(this string body, string parent, char delimiter, bool getNested = true)
        {
            char[] delis = getNested ? (parent + delimiter).ToCharArray() : new[] { delimiter };
            return GetChild(body, parent).Split(delis, StringSplitOptions.RemoveEmptyEntries);
        }
    }
}
