﻿using Sulakore.Habbo.Headers;
using Sulakore.Habbo.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BalloonFight
{
    public class Heartbeat
    {
        private Task HeartbeatTask;
        private TaskCreationOptions HeartbeatTaskOptions = TaskCreationOptions.LongRunning;
        private int tick = 0;

        public Heartbeat(HSession session)
        {
            HeartbeatTask = new Task(async () =>
            {
                while (session.IsConnected)
                {
                    await Task.Delay(45000);
                    await session.SendToServerAsync(Outgoing.Global.Ping, tick);
                    tick++;
                }
            }, HeartbeatTaskOptions);
        }

        public void StartHeartbeat()
        {
            if (HeartbeatTask.Status != TaskStatus.Running) HeartbeatTask.Start();
        }
    }
}
