﻿using Sulakore.Habbo.Headers;
using Sulakore.Habbo.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BalloonFight
{
    internal static class SessionExtensions
    {
        internal static void SendToServer(this HSession session, ushort header, params object[] chunks) =>
            session.SendToServerAsync(header, chunks).Wait();

        internal static void InitiateTrade(this HSession session, int virtualId) =>
            session.SendToServer(Outgoing.Global.TradePlayer, virtualId);

        internal static void RespectPlayer(this HSession session, int playerId) =>
            session.SendToServer(Outgoing.Global.RespectPlayer, playerId);

        internal static void NavigateRoom(this HSession session, int roomId) =>
            session.SendToServer(Outgoing.Global.NavigateRoom, roomId, false, false, -1);
    }
}
