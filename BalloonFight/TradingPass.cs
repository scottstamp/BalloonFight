﻿using Sulakore.Habbo.Web;
using System;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using System.Threading;

namespace BalloonFight
{
    public class TradingPass
    {
        private HSession session;
        private Random rnd;

        public TradingPass(HSession _session)
        {
            session = _session;
            rnd = new Random(session.SsoTicket.GetHashCode());
        }

        public void StartWandering(int x1, int x2, int y1, int y2)
        {
            new Task(async () =>
            {
                while (true)
                {
                    await session.SendToServerAsync(2513, rnd.Next(x1, x2), rnd.Next(y1, y2));
                    await Task.Delay(10000);
                }
            }).Start();
        }
    }
}
