﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PassHost
{
    public partial class Form1 : Form
    {
        private List<Process> Processes = new List<Process>();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            new Task(() =>
            {
                for (int x = 1; x <= 6; x++)
                {
                    for (int y = 1; y <= 3; y++)
                    {
                        Process p = new Process();
                        p.StartInfo = new ProcessStartInfo(@"C:\Users\balloons\Source\Repos\BalloonFight\BalloonFight\bin\x64\Release\BalloonFight.exe", $"{x * y}");
                        p.Start();
                        Processes.Add(p);
                    }

                    Task.Delay((int)TimeSpan.FromMinutes(90).TotalMilliseconds).Wait();
                    Processes.Clear();
                }
            }).Start();

            new Task(() =>
            {
                while (true)
                {
                    try {
                        if (Processes.Count == 6)
                        {
                            label1.Invoke((MethodInvoker)delegate
                            {
                                Processes[0].Refresh();
                                label1.Text = Processes[0].MainWindowTitle;
                            });
                            label2.Invoke((MethodInvoker)delegate
                            {
                                Processes[1].Refresh();
                                label2.Text = Processes[1].MainWindowTitle;
                            });
                            label3.Invoke((MethodInvoker)delegate
                            {
                                Processes[2].Refresh();
                                label3.Text = Processes[2].MainWindowTitle;
                            });
                            label4.Invoke((MethodInvoker)delegate
                            {
                                Processes[3].Refresh();
                                label4.Text = Processes[3].MainWindowTitle;
                            });
                            label5.Invoke((MethodInvoker)delegate
                            {
                                Processes[4].Refresh();
                                label5.Text = Processes[4].MainWindowTitle;
                            });
                            label6.Invoke((MethodInvoker)delegate
                            {
                                Processes[5].Refresh();
                                label6.Text = Processes[5].MainWindowTitle;
                            });
                        }
                    } catch { }
                    Task.Delay(1000).Wait();
                }
            }).Start();
        }
    }
}
